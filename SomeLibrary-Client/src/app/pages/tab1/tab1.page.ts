import { BookService } from './../../api/book.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  books: any = [];
  constructor(public bookService: BookService) {
    this.GetBookList();
  }

  GetBookList() {
    this.bookService.GetBook().subscribe((data) => {
      const anyData = data as any;
      this.books = anyData;
      console.log(this.books);
    });
  }

  printStatus(status) {
    if (status === 1) {
      return 'availabel';
    } else {
      return 'booked';
    }
  }

  ngOnInit() {
  }

}
