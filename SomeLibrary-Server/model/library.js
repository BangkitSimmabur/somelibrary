var pool = require('./databaseConfig.js');
var libraryDB = {
    getBook: function (callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            } else {
                console.log("Connected !");
                var sql = 'SELECT * FROM tb_book';
                conn.query(sql, function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },

    getAvailableBook: function (callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            } else {
                console.log("Connected !");
                var sql = 'SELECT * FROM tb_book where status = 1';
                conn.query(sql, function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },

    BorrowBook: function (status, book_id, title, date_borrowed, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                return callback(err, null);
            }
            else {
                var sql = 'INSERT INTO tb_borrow (borrow_id, book_id, title, date_borrowed, date_returned) VALUES (NULL,?,?,?, NULL)';
                conn.query(sql, [book_id, title, date_borrowed], function (err, result) {
                    if (err) {
                        return callback(err, null);
                    }
                    else {
                        var sql2 = 'UPDATE tb_book SET status = ? WHERE book_id=?';
                        conn.query(sql2, [status, book_id], function (err, result) {
                            conn.release();
                            if (err) {
                                console.log(err);
                                return callback(err, null);
                            } else {
                                console.log(result);
                                return callback(null, result);
                            }
                        });
                    }
                });
            }
        });
    },

    ChangeBookStatus: function (status, book_id, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                return callback(err, null);
            } else {
                var sql = 'UPDATE tb_book SET status = ? WHERE book_id=?';
                conn.query(sql, [status, book_id], function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },

    getBorrowedBook: function (callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                return callback(err, null);
            } else {
                var sql = 'SELECT * FROM tb_book where status = 0';
                conn.query(sql, function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },

};

module.exports = libraryDB