var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();
var path = require('path');
var cors = require("cors")
var cor = cors();
app.use(cor);
app.use(express.static(path.join(__dirname, "../public")));
var library = require('../model/library');

app.get('/api/library', function(req, res){
    library.getBook(function(err, result){
        if(!err) {
            res.send(result);
        } else {
            console.log (err);
            res.status(500).send(err.code);
        }
    });
});

app.get('/api/library/available', function(req, res){
    library.getAvailableBook(function(err, result){
        if(!err) {
            res.send(result);
        } else {
            console.log (err);
            res.status(500).send(err.code);
        }
    });
});

app.get('/api/library/borrowed', function(req, res){
    library.getBorrowedBook(function(err, result){
        if(!err) {
            res.send(result);
        } else {
            console.log (err);
            res.status(500).send(err.code);
        }
    });
});

app.post('/api/library/borrow_book', urlencodedParser, jsonParser, function (req, res){
    var book_id = req.body.book_id;
    var title = req.body.title;
    var date_borrowed = req.body.date_borrowed
    var status = 0;


    library.BorrowBook(status, book_id, title,date_borrowed, function (err, result){
        if (!err) {
            res.send(result.affectedRows + 'success borrowing a book')
        } else {
            res.status(500).send(err.code);
        }
    }); 
});

app.put('/api/library/changeborrow', urlencodedParser, jsonParser, function (req, res){
    var book_id = req.body.book_id;
    var status = 0;

    library.ChangeBookStatus(status, book_id, function (err, result){
        if (!err) {
            res.send(result.affectedRows + 'success borrowing a book')
        } else {
            res.status(500).send(err.code);
        }
    });
});

module.exports = app